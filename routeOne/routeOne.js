'use strict';

angular.module('myApp.routeOne', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
  var test = 'John';
  var template = '<div class="row">\
                    <div class="col-sm-3">column1 title</div>\
                    <div class="col-sm-2">column2 title</div>\
                    <div class="col-sm-4">column3 title</div>\
                    <div class="col-sm-1">column4 title</div>\
                  </div>\
                  <component-one ng-repeat="record in data track by $index" record="record" idx="{{$index}}"></component-one>\
                  <div directive-one one-way-data="data" two-way-data="data[1]" string-data="{{data[0].prop1}}"></div>';
  // var template = '<div class="row">\
  //                 <div class="col-sm-3">column1 title</div>\
  //                 <div class="col-sm-2">column2 title</div>\
  //                 <div class="col-sm-4">column3 title</div>\
  //                 <div class="col-sm-1">column4 title</div>\
  //               </div>\
  //               <component-one ng-repeat="record in data" record="record"></component-one>\
  //               <div directive-one data="data" ></div>';
$routeProvider.when('/routeOne', {
    template: template,
    controller: 'routeOneCtrl'
  });
}])

.controller('routeOneCtrl', ['$scope', 'myService', function ($scope, myService) {
  $scope.data = myService.dataGet();
  // myService.dataGet().then(function(data) {
  //     $scope.data = data;
  // }, function(errResponse){
  //     console.error('Error while fetching data');
  // });
}]);