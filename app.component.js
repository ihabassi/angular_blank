'use strict';

angular.module('myApp.componentOne', [])

.component("componentOne",{
    template:  '<div class="row">\
                    <div class="col-sm-3">{{dm.record.prop1}}</div>\
                    <div class="col-sm-2">{{dm.record.prop2}}</div>\
                    <div class="col-sm-4">{{dm.record.prop3}}</div>\
                    <div class="col-sm-1">{{dm.record.prop4}}</div>\
                    <div class="col-sm-2">\
                        <button type="button" class="btn btn-primary" ng-click="dm.primaryBtnFunc()">Primary</button>\
                        <button type="button" class="btn btn-danger" ng-click="dm.dangerBtnFunc()">danger</button>\
                    </div>\
                </div>',
    controller: function () {
        var dm = this;
        dm.$onInit = function () {
            dm.idx = Number(dm.idx);
            console.log(dm.record);
            console.log(dm.idx);
        };
        dm.primaryBtnFunc = function () {
            console.log('primary func button clicked');
        };

        dm.dangerBtnFunc = function () {
            console.log('danger func button clicked');
        };
    },
    bindings: {
        record: '<',
        idx: '@'
    },
    controllerAs: 'dm'
});