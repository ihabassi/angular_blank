'use strict';

angular.module('myApp.services', [])
.factory('myService', ['$http', '$q', function ($http, $q) {
	var data = [
		{
			prop1: 'prop1-1',
			prop2: 'prop1-2',
			prop3: 'prop1-3',
			prop4: 'prop1-4',
		},
		{
			prop1: 'prop2-1',
			prop2: 'prop2-2',
			prop3: 'prop2-3',
			prop4: 'prop2-4',
		},
		{
			prop1: 'prop3-1',
			prop2: 'prop3-2',
			prop3: 'prop3-3',
			prop4: 'prop3-4',
		},
		{
			prop1: 'prop4-1',
			prop2: 'prop4-2',
			prop3: 'prop4-3',
			prop4: 'prop4-4',
		},
		{
			prop1: 'prop5-1',
			prop2: 'prop5-2',
			prop3: 'prop5-3',
			prop4: 'prop5-4',
		}
	];
    return {
		dataGet: function () {
			return data;
			// return $http.get('data.json').then(function (response) {
			// 	return response.data;
			// }, function(errResponse){
			// 	console.error('Error while fetching data');
			// 	return $q.reject(errResponse);
			// });
		},
		 
	// 	recordAdd: function(record){
	// 		return $http.post('add url of the post data api', record).then(function (response) {
	// 			return response.data;
	// 		}, function(errResponse){
	// 			console.error('Error while creating Record');
	// 			return $q.reject(errResponse);
	// 		});
	// 	},
		 
	// 	recordUpdate: function(Record, id){
	// 		return $http.put('add url of the update Record api', Record).then(function(response) {
	// 			return response.data;
	// 		}, function(errResponse){
	// 			console.error('Error while updating Record');
	// 			return $q.reject(errResponse);
	// 		});
	// 	},
		 
	//    recordDelete: function(id){
	// 		return $http.delete('add the url of the delete data api').then(function (response) {
	// 			return response.data;
	// 		}, 
	// 		function(errResponse){
	// 			console.error('Error while deleting Record');
	// 			return $q.reject(errResponse);
	// 		});
	// 	}
	};
}]);