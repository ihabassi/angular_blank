'use strict';

angular.module('myApp.routeTwo', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  var template = '<div>this is controller two</div>';
  $routeProvider.when('/routeTwo', {
    template: template,
    controller: 'routeTwoCtrl'
  });
}])

.controller('routeTwoCtrl', [function() {
}]);