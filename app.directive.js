'use strict';

angular.module('myApp.directiveOne', [])

// // This directive declares an isolate scope.
// .directive('directiveOne', function() {
//     var directiveCTRL = function () {
//         var dm = this;
//         // dm.$onInit = function() {
//             console.log(dm.data);
//         // };
//     };
//     return {
//         strict: 'E',
//         binding: {
//             data: '=', // two way binding
//         },
//         controller: directiveCTRL,
//         controllerAs: 'dm',
//         bindToController: true,
//         template: '<div ng-repeat="record in dm.data">{{record.prop1}}</div>'
//     };
// });
.directive('directiveOne', [function () {
    var directiveCTRL = function () {
        var dm = this;
        dm.$onInit = function () {
            console.log('controller: ' + dm.twoWayData);
            console.log('controller: ' + dm.oneWayData);
            console.log('controller: ' + dm.stringData);
        };
    };
    return {
        restrict: 'A',
        controller: directiveCTRL,
        scope: {
            twoWayData: '=',
            oneWayData: '<',
            stringData: '@',
        },
        controllerAs: 'dm',
        bindToController: true
    };
}]);